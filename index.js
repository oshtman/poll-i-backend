var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const uuid = require('uuid/v5');

var polls = [];
const BASE_URL = 'localhost:3000'
const OPTIONS = ['0', '1/2', '1', '2', '3', '5', '8', '13'];

function createPoll(socket,name) {
  console.log('Create poll');
  const id = uuid.URL;
  const poll = {
    id: id,
    share_url: `${BASE_URL}/${id}`,
    users: [{
      id: socket.id, 
      answer: null
    }],
    answers: OPTIONS.reduce((map,option) => { map[option] = 0; return map }, {})
  };
  updatePoll(poll);
  socket.join(poll.id);
  io.to(poll.id).emit('poll:create',poll);
  emitAllPolls();
}

function joinPoll(pollId,socket) {
  console.log('Joining poll: ', pollId[0], socket.id);
  const poll = polls.find(p => p.id == pollId);
  if(poll == null) { return errored(socket,'No poll with the specified id found') };
  const user = poll.users.find(u => u.id === socket.id);
  if(user != null) { return errored(socket, 'User is already part of this poll') };
  const joiningUser = {
    id: socket.id, 
    answer: null
  }
  poll.users.push(joiningUser);
  updatePoll(poll);
  socket.join(poll.id);
  io.to(poll.id).emit('poll:join',poll);
}

function vote(socket,answer) {
  console.log('Vote in poll');
  const poll = polls.find(p => p.users.some(u => u.id === socket.id));
  if(poll == null) { return errored(socket,'No poll with the specified id found') };
  const user = poll.users.find(u => u.id === socket.id);
  if(user == null) { return errored(socket, 'User is not part of this poll') };
  if(!OPTIONS.includes(answer)) { return errored(socket, 'Answer not part of result set') };
  if(user.answer != null) { 
    poll.answers[user.answer] -= 1;
  }
  poll.answers[answer] += 1;
  user.answer = answer;
  updateUser(user,poll);
  updatePoll(poll);
  io.to(poll.id).emit('poll:vote',poll);
}

function updateUser(user,poll) {
  const index = poll.users.findIndex(u => u.id === user.id);
  if(index === -1) {
    poll.users.push(user)
  }else {
    poll.users[index] = user;
  }
}

function updatePoll(poll) {
  const index = polls.findIndex(p => p.id === poll.id);
  if(index === -1) {
    polls.push(poll);
  }else {
    polls[index] = poll;
  }
}

function userLeftPoll(socket) {
  const index = polls.findIndex(p => p.users.some(u => u.id === socket.id));
  console.log('Index of poll that user belongs to: ', index);
  if(index != -1) {
    const poll = polls[index];
    const userIndex = poll.users.findIndex(u => u.id === socket.id);
    console.log('Index of user in polls user list: ', userIndex);
    if(userIndex != -1) { 
      poll.users.splice(userIndex,1);
      if(poll.users.length === 0) {
        polls = polls.filter(p => p.id != poll.id);
      }else {
        polls[index] = poll;
      }
      console.log('Emitting poll update due to user leaving: ', poll);
      io.to(poll.id).emit('poll:update', poll);
    };
  }
}

function emitAllPolls() {
  io.emit('polls:all',polls);
}

function errored(socket,msg) {
  socket.emit('message',msg);
}

io.on('connection', function(socket){
  console.log('A user connected');
  socket.on('poll:create', (name) => createPoll(socket,name));
  socket.on('poll:join', (pollId) => joinPoll(pollId,socket));
  socket.on('poll:vote', (answer) => vote(socket,answer));
  socket.on('polls:all', () => emitAllPolls());

  socket.on('disconnect', function() {
    console.log('A user disconnected');
    userLeftPoll(socket);
  });
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});